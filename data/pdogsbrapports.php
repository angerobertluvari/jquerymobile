<?php

/**
 * Classe d'accès aux données. 

 * Utilise les services de la classe PDO
 * pour l'application Gsb Rapport Mobile
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO 
 * $monPdoGsbRapports qui contiendra l'unique instance de la classe
 * @package default
 * @author Cheri Bibi
 * @version    1.0
 * @link       http://www.php.net/manual/fr/book.pdo.php
 */
class PdoGsbRapports {

    private static $serveur = 'mysql:host=localhost'; // A compléter
    private static $bdd = 'dbname=GSBRapports';
    private static $user = 'userGsb';
    private static $mdp = 'mdpGsb';
    private static $monPdo;
    private static $monPdoGsbRapports = null;

    /**
     * Constructeur privé, crée l'instance de PDO qui sera sollicitée
     * pour toutes les méthodes de la classe
     */
    private function __construct() {
        self::$monPdo = new PDO(self::$serveur . ';' . self::$bdd, self::$user, self::$mdp);
        self::$monPdo->query("SET CHARACTER SET utf8");
    }

    public function _destruct() {
        self::$monPdo = null;
    }

    /**
     * Fonction statique qui crée l'unique instance de la classe
     * Appel : $instancePdoGsbRapports = PdoGsbRapports::getPdo();
     * @return l'unique objet de la classe PdoGsbRapports
     */
    public static function getPdo() {
        if (self::$monPdoGsbRapports == null) {
            self::$monPdoGsbRapports = new PdoGsbRapports();
        }
        return self::$monPdoGsbRapports;
    }

    /**
     * Retourne les informations du visiteur
     * @param $login 
     * @param $mdp
     * @return le tableau associatif ou NULL
     */
    public function getLeVisiteur($login, $mdp) {
        $req = "SELECT id, nom, prenom FROM visiteur WHERE login = :login AND mdp = :mdp"; // A compléter
        $stm = self::$monPdo->prepare($req);
        $stm->bindParam(':login', $login);
        $stm->bindParam(':mdp', $mdp);
        $stm->execute();
        $laLigne = $stm->fetch();
        if (count($laLigne) > 1)
            return $laLigne;
        else
            return NULL;
    }

    public function getLesMedecins($nom) {
        $req = "SELECT id, nom, prenom, adresse, tel FROM medecin WHERE nom LIKE '" . $nom . "%' ORDER BY nom, prenom"; // A compléter
        $rs = self::$monPdo->query($req);
        $lesLignes = $rs->fetchAll();
        return $lesLignes;
    }

    public function getLeMedecin($idMedecin) {
        $req = "SELECT * FROM medecin WHERE id = :idMedecin"; // A compléter
        $stm = self::$monPdo->prepare($req);
        $stm->bindParam(':idMedecin', $idMedecin);
        $stm->execute();
        $laLigne = $stm->fetch();
        return $laLigne;
    }

    function majMedecin($id, $adresse, $tel, $specialite) {
        $req = "UPDATE medecin SET tel = :tel ,adresse = :adresse, specialiteComplementaire = :specialite WHERE id = :idMedecin;";
        $stm = self::$monPdo->prepare($req);
        $stm->bindParam(':idMedecin', $id);
        $stm->bindParam(':specialite', $specialite);
        $stm->bindParam(':tel', $tel);
        $stm->bindParam(':adresse', $adresse);
        return $stm->execute();
    }

    function getLesRapports($idMedecin) {
        $req = "select motif, date, bilan, visiteur.nom, visiteur.prenom";
        $req .= " from rapport join medecin on rapport.idMedecin = medecin.id";
        $req .= " join visiteur on rapport.idVisiteur = visiteur.id";
        $req .= " where medecin.id = :idMedecin order by date";
        $stm = self::$monPdo->prepare($req);
        $stm->bindParam(':idMedecin', $idMedecin);
        $stm->execute();
        $lesLignes = $stm->fetchall();
        return $lesLignes;
    }

}

// fin classe
?>


