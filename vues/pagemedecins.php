<div data-role = "page" data-theme="A" id = "pagemedecins">
    <?php
        include "vues/entetepagemedecins.html";
    ?>   
    
    
 <div data-role = "content">
        <label for="#listeMedecins">Recherche de médecin par nom</label>
        <ul id="listeMedecins" data-role="listview" data-inset="true" data-filter="true" data-filter-placeholder="Chercher un medecin.." data-filter-theme="a"></ul>
        <label for="medecin">Nom médecin</label>
        <input type="text" name="name" id="medecin" value=""  />
    </div> <!-- /fin content -->
    
    <?php
        include "vues/piedpage.html";
    ?>
</div><!-- /fin page -->

